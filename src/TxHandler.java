import java.util.HashSet;
import java.util.Set;

public class TxHandler {


    private UTXOPool utxoPool;
    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
        this.utxoPool = new UTXOPool(utxoPool);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        // rule no. (1) (4) (5/2)
        double outputSum = 0;
        for (Transaction.Output out : tx.getOutputs()){
            if(out.value <0)
                return false;
            outputSum += out.value;

        }

        //rule
        int index =0;
        double inputSum = 0;
        HashSet<UTXO> seen = new HashSet<UTXO>();
        for(Transaction.Input t : tx.getInputs()) {
            UTXO ut = new UTXO(t.prevTxHash, t.outputIndex);

            if (!utxoPool.contains(ut))
                return false;

            Transaction.Output output = utxoPool.getTxOutput(ut);

            byte[] rawDataToSign = tx.getRawDataToSign(index++);
            //(2)
            if(!Crypto.verifySignature(output.address,rawDataToSign,t.signature))
                return false;

            //(3)
            if(seen.contains(ut))
                return false;
            seen.add(ut);
            inputSum+=output.value;
        }

        return inputSum >= outputSum;
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        Set<Transaction> validTrans = new HashSet<Transaction>();
        for(Transaction psTrans: possibleTxs){
            if(isValidTx(psTrans)){
                validTrans.add(psTrans);

                for (Transaction.Input in : psTrans.getInputs()) {
                    UTXO utxo = new UTXO(in.prevTxHash, in.outputIndex);
                    utxoPool.removeUTXO(utxo);
                }

                for (int i = 0; i < psTrans.numOutputs(); i++) {
                    utxoPool.addUTXO(new UTXO(psTrans.getHash(), i), psTrans.getOutput(i));
                }

            }
        }

        return validTrans.toArray(new Transaction[validTrans.size()]);
    }

}
