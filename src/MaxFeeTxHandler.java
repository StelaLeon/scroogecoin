import java.lang.reflect.Array;
import java.util.*;


public class MaxFeeTxHandler {

    public class SortByMaxFee implements Comparator<Transaction> {

        @Override
        public int compare(Transaction o1, Transaction o2) {
            double o1Fee = getTotalInputs(o1)-getTotalOutputs(o1);
            double o2Fee = getTotalInputs(o2)-getTotalOutputs(o2);

            return o2Fee-o1Fee > 0 ? 1 : o1Fee == o2Fee ?0 :-1;
        }

    }

    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        Arrays.sort(possibleTxs, new SortByMaxFee());

        HashSet<Transaction> seen = new HashSet<Transaction>();

        for(Transaction tx: possibleTxs){
            if(isValidTx(tx)){
                seen.add(tx);

                for (Transaction.Input i : tx.getInputs())
                    utxoPool.removeUTXO(new UTXO(i.prevTxHash, i.outputIndex));
                int index = 0;
                for (Transaction.Output o : tx.getOutputs())
                    utxoPool.addUTXO(new UTXO(tx.getHash(), index++), o);
            }
        }
        return seen.toArray(new Transaction[seen.size()]);
    }

        private double getTotalInputs(Transaction in) {
        double sum = 0;
        for(Transaction.Input t: in.getInputs()){
            UTXO ut = new UTXO(t.prevTxHash,t.outputIndex);

            if(utxoPool.contains(ut)&& isValidTx(in))
                sum+=utxoPool.getTxOutput(ut).value;
        }

        return sum;
    }

    private double getTotalOutputs(Transaction out){
        double sum = 0;

        for(Transaction.Output o: out.getOutputs()){
            sum+=o.value;
        }

        return sum;
    }



    private UTXOPool utxoPool;
    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public MaxFeeTxHandler(UTXOPool utxoPool) {
        this.utxoPool = new UTXOPool(utxoPool);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool,
     * (2) the signatures on each input of {@code tx} are valid,
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        // rule no. (1) (4) (5/2)
        double outputSum = 0;
        for (Transaction.Output out : tx.getOutputs()){
            if(out.value <0)
                return false;
            outputSum += out.value;

        }

        //rule
        int index =0;
        double inputSum = 0;
        HashSet<UTXO> seen = new HashSet<UTXO>();
        for(Transaction.Input t : tx.getInputs()) {
            UTXO ut = new UTXO(t.prevTxHash, t.outputIndex);

            if (!utxoPool.contains(ut))
                return false;

            Transaction.Output output = utxoPool.getTxOutput(ut);

            byte[] rawDataToSign = tx.getRawDataToSign(index++);
            //(2)
            if(!Crypto.verifySignature(output.address,rawDataToSign,t.signature))
                return false;

            //(3)
            if(seen.contains(ut))
                return false;
            seen.add(ut);
            inputSum+=output.value;
        }

        return inputSum >= outputSum;
    }

}

